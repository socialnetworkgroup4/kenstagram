<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>

<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
      
            <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
       <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <h1>Register with us</h1>
        <?php
		
		
		
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
      
        <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" 
               method="post" 
                name="registration_form">
                
               
       <div text="center">
                      
      <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">perm_identity</i>
      <input type='text' 
              name='username' 
                id='username' />
          <label for="icon_prefix">Username</label>
          
        </div>
         </div>
        
        <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">email</i>
         <input type="text" name="email" id="email" />
          <label for="icon_telephone">Email</label>
        </div>
		</div>
       <div class="row">
     
        <div class="input-field col s6">
          <i class="material-icons prefix">info_outline</i>
        <input type="password"
                             name="password" 
                             id="password"/>
       
          <label for="icon_prefix">password</label>
        </div>
        
      </div>
    

   
    
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">info_outline</i>
        <input type="password" 
                                     name="confirmpwd" 
                                     id="confirmpwd" />
          <label for="icon_telephone">confirmpassword</label>
        </div>
      </div>
   </div>
    
<a class="waves-effect waves-light btn" style="background-color:‪#‎FF8000">  <input type="button" 
                   value="Register" 
                   onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd);"  /> </a>
    </form>
  </div></div>
    </div>
  </div></div>

   </div>
    </body>
</html>
</body>
</html>