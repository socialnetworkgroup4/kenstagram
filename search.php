

<?php
	include "config.php";
  include "session_load.php";
  include 'head.php';
	$word = $_POST['search'];
	$query =  "SELECT * FROM profile WHERE (name LIKE '%$word%' or email LIKE '%$word%')";
	$conn->query($query);
	$result = $conn->query($query);
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
 <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
<link href="style/style.css" rel="stylesheet">
<title>Kenstagram</title>

</head>

<body>
	<div class="container">
	<?php
	while($row = $result->fetch_object())
    {
        $id_user =$row->id_username;
        $images = $row->display;
        $name = $row->name;
        $bio = $row->bio;
    

	?>
	  <div class="row">
          <div class="col s12 m4">
            <h4 class="light"><?php echo $name;?></h4>
            <div class="card">
              <div class="card-image">
                <img src="<?php echo $images;?>"style="width: 300px;height: 180px;">
                <span class="card-title">Card Title</span>
              </div>
              <div class="card-action" align="center">
            <?php if($id_user!= $id_login)
            {
            ?>
            <form method="post" action="add_friend_insert.php">
	      		<input type="hidden" name="id_user" value="<?php echo $id_user;?>">
            <button style="background-color: #41B03C" class="btn waves-effect waves-light" type="submit" name="action">Add Friend
          <i class="material-icons right" >add_circle</i>
	  			</button>
			  </form>
        <?php } else { ?>
        <button style="background-color: #EE3E3E" class="btn waves-effect waves-light" name="action">Yourself
        <i class="material-icons right">remove_circle</i>
        <?php } ?>
              </div>
            </div>
          </div>

        </div>

        <?php } ?>
        </div>
	<!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>
