<?php 
include 'profile-load.php';
//include 'config.php';
?>
<html>
	<title>Kenstagram</title>
<body class="grey lighten-5">
	<div class="container">
		<div class="col s12 m8 offset-m2 l6 offset-l3" style="margin-top:5%;margin-bottom:5%;">
			<div class="grey lighten-5">
				<div class="row valign-wrapper">
					<div class="col s3 m4 ">
					  	<img src="<?php echo $display ?>" alt="" class="circle responsive-img">
					</div>
					
				</div>
                <div class="col s9 m5 push-m1">
						<div class="row">
							<div class="col s7 m6 l6"><H4><?php echo $name ?></H4></div>
							
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col s12 m12 l12"><span><?php echo $bio ?></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
							<div class="col s2 m2 l2"><span><?php echo $sex ?></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col s2 m2 l2"><span>Age: <?php echo $age ?></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col s4 m4 l4"><span><?php echo $post_num ?></span> posts</div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col s4 m4 l4"><span><?php echo $friend_num ?></span> Friends</div>
						</div>
						<div class="row">
							<div class="col s4 m4 l4"><span><?php echo $email ?></span></div>
						</div>
                        <div class="row">
							<a href="profile.php" class="small waves-effect waves-light btn" style="background-color:#0C3">View</a>
						</div>
                        <div class="row">
							<a href="noti_friend.php" class="small waves-effect waves-light btn" style="background-color:#0C3">Friends request</a>
						</div>
					</div>
			</div>
		</div>
    </div>
</body>
</html>
        