<!doctype html>
<html>
<head><title>Kenstagram</title>
<meta charset="utf-8">
  <link rel="stylesheet" href="css/profilecss.css" />
     <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
         <!-- Font Kenstagram-->
<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<!-- Font Basic-->
<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
<meta charset="utf-8">
</head>

<body>
<!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      
         <nav>
      <div class="nav-wrapper" style="background-color:#FF8000;">
      <a href="feed.php" class="brand-logo" style="margin-left:20px; font-family:Pacifico;">Kenstagram</a>
      <ul class="right hide-on-med-and-down">  
        <div class="nav-wrapper">
       <div class="row">
       <a href="includes/logout.php" class="small waves-effect waves-light btn" style="background-color:#0C3">Logout</a>
    <div class="input-field col s8">
      <form method="post" action="search.php">
        <div class="input-field">
          <input name="search" type="search" required>
          <label for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
      
    </div>
     </div>
  </div>
   </div>
  </nav>
</body>
</html>