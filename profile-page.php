<?php 
include 'profile-load.php';
include 'config.php';

?>
<html>
	<head>
		<!--Import Google Icon Font-->	 
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import jQuery before materialize.js-->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    	<script type="text/javascript" src="bower_components/Materialize/dist/js/materialize.min.js"></script>
    	<link type="text/css" rel="stylesheet" href="bower_components\Materialize\dist\css\materialize.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body class="grey lighten-5">
	<div class="container">
		<div class="col s12 m8 offset-m2 l6 offset-l3" style="margin-top:5%;margin-bottom:5%;">
			<div class="grey lighten-5">
				<div class="row valign-wrapper">
					<div class="col s3 m4 ">
					  	<img src="<?php echo $display ?>" alt="" class="circle responsive-img">
					</div>
					<div class="col s9 m5 push-m1">
						<div class="row">
							<div class="col s7 m6 l6"><H4><?php echo $name ?></H4></div>
							<div class="col s5 m6 l6" style="padding:18px;"><a href="profile-edit.php" class="small waves-effect waves-light btn" style="background-color:#0C3">EDIT</a></div>
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col s12 m12 l12"><span><?php echo $bio ?></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
							<div class="col s4 m4 l4"><span><?php echo $sex ?></span></div>
                            <div class="col s4 m4 l4"><span>Age: <?php echo $age ?></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col s4 m4 l4"><span><?php echo $post_num ?></span> posts</div>
                            <div class="col s4 m4 l4"><span><?php echo $friend_num ?></span> Friends</div>
						</div>
						<div class="row">
							<div class="col s4 m4 l4"><span><?php echo $email ?></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
			<div class="row" style="margin-bottom:2%;">
    	<?php 
			$query = mysqli_query($conn, "SELECT * FROM post WHERE id_user = '$id_login'");
			$rows = mysqli_num_rows($query);
			if($rows > 0)
			{
				while($rows = mysqli_fetch_assoc($query)){
					$count = 0;
					if($username == $rows['id_user'] && $count < 3)
					{
						$pic = $rows['image'];
						echo '<div class="col s12 m4" style="height:250px;">
								<div class="card">
									<div class="card-image">
										<img style="height:250px;" src="'.$pic.'">
									</div>
								</div>
							 </div>';
							 $count++;
					}
				}
			}
			mysqli_close($conn);
		?>
		
			</div>
			

		</div>
	</div>	
</body>
</html>