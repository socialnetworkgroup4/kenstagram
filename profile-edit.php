<?php 
include 'config.php';
include 'profile-edit-load.php';
?>
<!doctype html>
<html>
<head>

</head>

<body>
  <?php include 'head.php' ?>
  
  
    
<form action="" method="post" enctype="multipart/form-data">
	<div class="container">
		<div class="col s12 m8 offset-m2 l6 offset-l3" style="margin-top:5%;margin-bottom:5%;">
			<div class="grey lighten-5">
				<div class="row valign-wrapper">
					<div class="col s3 m4 ">
					  	<img src="<?php echo $display ?>" alt="" class="circle responsive-img">
                        <div class="file-field input-field">
      						<div class="btn">
    						    <span>File</span>
       								 <input type="file" name="file">
                                
      						</div>
                            
      						<div class="file-path-wrapper">
      					  	<input class="file-path validate" type="text">
                            <span><input class="small waves-effect waves-light btn" style="background-color:#0C3" type="submit" value="Up" name="upload"></span>
      					</div>
    				</div>
					</div>
					<div class="col s9 m5 push-m1">
						<div class="row">
							<div class="col s7 m6 l6"><H4><input type="text" value="<?php echo $name ?>" name="name"></H4></div>
							<div class="col s5 m6 l6" style="padding:18px;">
                            <input class="small waves-effect waves-light btn" style="background-color:#0C3" type="submit" value="Save" name="save"></div>
						</div>
						<div class="row" style="margin-bottom:10px;">
							<div class="col s12 m12 l12"><span><textarea name="bio"><?php echo $bio ?></textarea></span></div>
						</div>
                        <div class="row" style="margin-bottom:10px;">
							<div class="col s4 m4 l4"><span>Sex :  <input type="text" value="<?php echo $sex ?>" name="sex"></span></div>
                            <div class="col s4 m4 l4"><span>Age: <input type="text" value="<?php echo $age ?>" name="age"></span></div>
						</div>
                        
						<div class="row">
							<div class="col s4 m4 l4"><span><input type="text"  value="<?php echo $email ?>" name="email"></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>	
    </form>
</body>
</html>




